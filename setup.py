'''
Quart-Login
-----------
Quart-Login provides user session management for Quart. It handles the common
tasks of logging in, logging out, and remembering your users'
sessions over extended periods of time.
Quart-Login is not bound to any particular database system or permissions
model. The only requirement is that your user objects implement a few
methods, and that you provide a callback to the extension capable of
loading users from their ID.
Links
`````
* `documentation <https://quart-login.readthedocs.io/en/latest/>`_
* `development version <https://gitlab.com/jamieoglindsey0/quart_login>`_
'''
import os
import sys

from setuptools import setup

about = {}
with open('quart_login/__about__.py') as f:
    exec(f.read(), about)

if sys.argv[-1] == 'test':
    status = os.system('make check')
    status >>= 8
    sys.exit(status)

setup(name=about['__title__'],
      version=about['__version__'],
      url=about['__url__'],
      license=about['__license__'],
      author=about['__author__'],
      author_email=about['__author_email__'],
      description=about['__description__'],
      long_description=__doc__,
      long_description_content_type='text/markdown',
      packages=['quart_login'],
      zip_safe=False,
      platforms='any',
      install_requires=['quart'],
      classifiers=[
        'Development Status :: 1 - Beta',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: Implementation :: CPython',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
        ])